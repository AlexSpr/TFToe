import numpy as np
import random
import copy
import decimal  
import sys
import argparse
import os

import CNN as NN
from TicTacToeGame import TICTACTOEGAME

class Play_TFTOE():
    def __init__(self, name = "Model"):
        self.winner_dict = { 1 : "Neural Net", 0 : "No one", -1: "Player" }
        self.rowIdentity = ['a','b','c']

        self.board_width = 3
        self.board_height = 3

        self.tile_states = [1,0,-1]
        self.tile_states_num = len(self.tile_states)

        self.bitarray_size = self.board_width * self.board_height * len(self.tile_states)

        self.model_name = name 

        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

    def player(self, ttt_game):
        text = str(input("Please make move (such as a1, b3, etc ...):"))
        try: #if input is correct 
            row = self.rowIdentity.index(text[0])
            column = int(text[1]) - 1

            if ttt_game.isMoveViable(row, column) == True:	# check if move is viable 
                return row, column 
            else:
                print("please choose an available field") # else make new move 
                return self.player(ttt_game)
        except: 	# else make new move 
            print("please choose an available field")
            return self.player(ttt_game)

    def player_game(self):
        nn = NN.MODEL((3,3,3),9, name = self.model_name) #CNN input (3,3,3) #NN input 27

        ttt = TICTACTOEGAME()
        ttt.initilize_game()

        if (nn.load()):
            print("Loaded Model")
        else:
            raise Exception("Model to load not found")

        ttt.printCurrentBoard()
        while not ttt.gameOver:
            if ttt.to_move == 1: # if it's nn move
                print("NN trun")
                action = nn.make_valid_prediction(nn.convert_to_bitinput(ttt.get_board()))
                action2 = None

            else:
                print("Player trun")
                action, action2 = self.player(ttt)

            ttt.make_move(action, action2) #adds action to board and switches ttt.to_move to other player     
            ttt.printCurrentBoard()
        
        print("GAME OVER")
        print(self.winner_dict[ttt.get_winner()],"is the winner!")
        nn.terminate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-name', "--model_name" , help = 'Name of Model', required = False, default = "Model")
    
    args = parser.parse_args()
    model_name = str(args.model_name)

    tftoe = Play_TFTOE(model_name)
    tftoe.player_game()
