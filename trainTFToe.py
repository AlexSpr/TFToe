import numpy as np 
import math
import random
import copy
import decimal  
#import seaborn as sns 
import argparse
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys
import os

import neuralnets.nn as NN
from trainingtools.memory import Memory
from trainingtools.plot import Plot
from tictactoe.alphabeta import AlphaBeta
from tictactoe.TicTacToeGame import TICTACTOEGAME

'''
s #state of enviroment | current board
a #action | pick one of the 9 fields 

new_s #new state | updated board 
r #reward for taken action

v #value estimation This value estimation should correspond to the discounted expected reward over time from taking action (a) while in state (s)
'''
                
class TFToeTrainer:

    def __init__(self, name = "Model"):
        self.mem_size = 7540 #5250#
        self.game_memory = Memory(self.mem_size)  #amount of moves stored in memory

        self.batch_size = 60  #training batch size

        self.epochs = 250   # num of epochs 
        self.matches_per_epoch = 50 # number of games played during one epoch
        self.iterations = 1

        # greedy policy for selecting an action
        # the higher the value of self.train_epsilon the higher the probability of an action being random.     
        self.epsilon = 1.0  #epsilon value
        self.epsilon_min = 0.0  #minimum epsilon value 
        self.epsilon_min_epoch = 200    #epoch at which epsilon minimum should be reached 
        self.epsilon_downrate = self.calculate_downrate()  #epsilon decremnt value after each epoch 

        self.train_epsilon = self.epsilon  

        self.reward_mul = 75

        self.GAMMA = 0.95    # Discount factor -- determines the importance of future rewards

        #'o' = 1 -> nn
        #'x' = -1 -> oponent

        self.model_name = name
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        self.results_path = os.path.join(THIS_FOLDER, "results") 

        #os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

    def train_neural_network(self, new_nn = False, show_plot = True, save_plot = False, test_games = 100):
        nn = NN.Model(27,9, name = self.model_name, make_new = new_nn) #CNN input (3,3,3) #NN input 27
        plot = Plot()
        
        print("trying to load model")
        if (nn.load()):
            print("Loaded Model")
        else:
            print("Created new Model")
        '''
        nn.print_var()
        '''
        self.train_epsilon = self.epsilon
        max_tie_perc = 0     
        
        for epoch in range(self.epochs):
            epoch_loss = 0

            won_games  = 0
            lost_games = 0  
            draw_games = 0

            for match in range(self.matches_per_epoch): #to be changed to episodes 
                print("Epoch:", epoch + 1," Match:", match + 1, "\tof", self.matches_per_epoch, end = '\r')
                reward = self.play_training_game(nn)

                if reward == 1:
                    won_games += 1
                elif reward == -1:
                    lost_games += 1
                else:
                    draw_games += 1 
                
                replay_loss = self.replay(nn)
                epoch_loss += replay_loss

            won_perc = won_games / self.matches_per_epoch
            lost_perc = lost_games / self.matches_per_epoch
            draw_perc = draw_games / self.matches_per_epoch
            '''
            if (draw_perc >= max_tie_perc):
                nn.save()
                max_tie_perc = draw_perc
            '''
            #print("win %:", won_perc ,"\tloss %:", lost_perc ,"\ttie %:", draw_perc, "\t total games:", self.matches_per_epoch, "\tEpochs:", epoch + 1, "\tdownrate:", round(self.train_epsilon,4))
            plot.update_values(won_perc, lost_perc, draw_perc, self.train_epsilon, epoch_loss)

            if self.train_epsilon - self.epsilon_downrate > self.epsilon_min:
                self.train_epsilon -= self.epsilon_downrate
            else: 
                self.train_epsilon = self.epsilon_min

        '''
        nn.print_var()
        '''
        print("Saving Model")
        nn.save()
        

        test_run_dict = self.tftoe_test_games(nn, num_games = test_games)
        #nn.terminate() #kills session of network

        plot.update_dynamic()
        
        if show_plot:
            plot.keep_open()

        if save_plot:
            name = "T" + str(test_run_dict[0]) + "_G" + str(test_run_dict["games"]) + "_%s" +  ".png"
            fig_path = os.path.join(self.results_path, name)
            i = 0
            while os.path.exists(fig_path % i):
                i += 1
            fig_path = fig_path % i

            plot.save(fig_path)

        return test_run_dict
        
        
    def replay(self, nn):
        batch = self.game_memory.get_sample(self.batch_size)

        states = np.array([val[0] for val in batch])
        next_states = np.array([(np.zeros(nn.input_size) if val[3] is None else val[3]) for val in batch])

        q_s_a = nn.get_batch_Q_values(states.astype('float32')).numpy()
        q_s_a_d = nn.get_batch_Q_values(next_states.astype('float32')).numpy()

        batch_x = []
        batch_y = []

        for i, move in enumerate(batch):
            state, action, reward, next_state = move[0], move[1], move[2], move[3]

            current_q = q_s_a[i]
            
            if next_state is None:
                current_q[action] = reward
            else:
                current_q[action] = reward + self.GAMMA * np.amax(q_s_a_d[i])

            batch_x.append(state)
            batch_y.append(current_q)

        t_loss = nn.train_batch_step(batch_x, batch_y)
        return t_loss


    def play_training_game(self, nn): #punishes for false moves
        memory = [] # memory = [pre_move_state, action, reward, post_move_state]
        ttt = TICTACTOEGAME()
        ttt.initilize_game()

        while not ttt.gameOver:
            ttt.rotate_board()

            if ttt.to_move == 1: # if it's training nn move
                del memory[:]   # clear memory 
                #print('board',ttt.get_board())
                bit_board = nn.convert_to_bitinput(ttt.get_board()) #represents board that is input into nn
                #print('board state fresh', bit_board)
                memory.append(np.copy(bit_board))   #current state added to memory 

                if random.random() > self.train_epsilon:                  #if epsilon is smaller than random value, network makes a move
                    #print("type", bit_board.dtype, bit_board)
                    #print('board state', bit_board)
                    action = nn.make_valid_prediction(bit_board)
                else:                                               #otherwise a random move from all possible viable moves is chosen
                    action = random.choice(ttt.get_viable_moves())

                memory.append(action)     #add action to memory 
                
            else:
                #print('inverse board',np.negative(ttt.get_board()))
                inverse_bit_board = nn.convert_to_bitinput(np.negative(ttt.get_board()))
                #print('inverse board state fresh', inverse_bit_board)
                if random.random() > self.train_epsilon:                  #if epsilon is smaller than random value, network makes a move
                    #print("type", inverse_bit_board.dtype, inverse_bit_board)
                    #print('inverse board state', inverse_bit_board)
                    action = nn.make_valid_prediction(inverse_bit_board)
                else:                                               #otherwise a random move from all possible viable moves is chosen
                    action = random.choice(ttt.get_viable_moves())

            #print(action)
            #ttt.printCurrentBoard()
            post_move_board, reward = ttt.make_move(action) #adds action to board and switches ttt.to_move to other player

            if (ttt.to_move == 1 and ttt.num_of_moves > 1) or ttt.gameOver:
                memory.append(reward * self.reward_mul)

                if ttt.gameOver:
                    memory.append(None)
                else:
                    memory.append(np.copy(nn.convert_to_bitinput(post_move_board)))

                self.game_memory.add_sample(memory)

        return reward

    def tftoe_find_perfect_player(self, save_plots = False):
        if save_plots:
            dir_name = self.model_name + "_%s"        

            self.results_path = os.path.join(self.results_path, dir_name) 
            i = 0
            while os.path.exists(self.results_path % i):
                i += 1
            self.results_path = self.results_path % i
            os.makedirs(self.results_path)

        test_games = 1000

        training_runs = 0 
        while True:
            print("Training Run:", training_runs + 1)
            test_outcomes = self.train_neural_network(new_nn = True, show_plot = False, save_plot = save_plots, test_games = 1000)
            if test_outcomes[0] == test_games:
                break
            training_runs += 1

        print("Perfect NN player has been found after %i training runs" % (training_runs + 1))

    def tftoe_training_runs(self, runs = 1, new_nn = False, save_plots = True, show_plots = False):
        if save_plots:
            dir_name = self.model_name + "_%s" + "_Rx" +  str(self.reward_mul) + "_GA" + str(self.GAMMA) + "_E" + str(self.epochs) + "_EM" + str(self.matches_per_epoch) + "_B" + str(self.batch_size) + "_MEM" + str(self.mem_size)        

            self.results_path = os.path.join(self.results_path, dir_name) 
            i = 0
            while os.path.exists(self.results_path % i):
                i += 1
            self.results_path = self.results_path % i
            os.makedirs(self.results_path)
        
        for run in range(runs):
            print("Run", run + 1,"of", runs )
            self.train_neural_network(new_nn, show_plot = show_plots, save_plot = save_plots)

    def tftoe_test_games(self, nn, num_games = 100):
        test_run_dict = { 1 : 0, 0 : 0, -1: 0, "games": num_games}
        for _ in range(num_games):
            test_run_dict = self.play_ab_test_game(nn, test_run_dict)
        print('\nLosses:', test_run_dict[-1], '\tTies:', test_run_dict[0], '\tGames:', num_games)
        return test_run_dict

    def play_ab_test_game(self, nn, test_run_dict):
        ab = AlphaBeta()
        ttt = TICTACTOEGAME()
        ttt.initilize_game()

        while not ttt.gameOver:
            ttt.rotate_board()
            if ttt.to_move == 1: # if it's nn move
                action = nn.make_valid_prediction(nn.convert_to_bitinput(ttt.get_board()))
                action2 = None 
            else:
                action, action2 = ab.choose_move(np.negative(ttt.get_board()))

            ttt.make_move(action, action2) #adds action to board and switches ttt.to_move to other player
        test_run_dict[ttt.get_winner()] += 1
        
        return test_run_dict

    def calculate_downrate(self):
        return (self.epsilon - self.epsilon_min) / self.epsilon_min_epoch

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-new', action = "store_true" , help = 'Create new nn to train', required = False, default = False)
    parser.add_argument('-save_plots', action = "store_true" , help = 'Save plots to folder?', required = False, default = False)
    parser.add_argument('-find_perfect', action = "store_true" , help = 'Train till perfect player is found', required = False, default = False)
    parser.add_argument('-runs', "--training_runs" , help = 'How often should the nn be trained', required = False, default = 1)
    parser.add_argument('-name', "--model_name" , help = 'Name of Model', required = False, default = "Model")
    
    args = parser.parse_args()
    new_nn = args.new
    runs = int(args.training_runs)
    perfect = args.find_perfect
    save_plots = args.save_plots
    model_name = str(args.model_name)


    tftoe = TFToeTrainer(model_name)

    if perfect:
        tftoe.tftoe_find_perfect_player(save_plots)
    else: 
        tftoe.tftoe_training_runs(runs, new_nn)