import numpy as np
import os
import shutil

from neural_network.TFmodel import TF_MODEL

class MODEL():
    def __init__(self, input_size, output_size, name = None, make_new = False):

        self.qmodel = self.make_dense_model(input_size, output_size, "qmodel")

        self.tmodel = self.make_dense_model(input_size, output_size, "tmodel")

        self.tau = 0.01

        self.copy_op = self.qmodel.copy_operation("qmodel", "tmodel", self.tau)

        self.qmodel.init()
        self.tmodel.init()

        self.input_size = input_size
        self.output_size = output_size

        self._model_name = None
        if name is None:
            self._model_name = "neural_net"
        else:
            self._model_name = name

        self.tile_states = [1,0,-1]

        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        model_path = os.path.join(THIS_FOLDER, "neural_network/saved_models") 

        self.saved_model_dir = os.path.join(model_path, self._model_name)

        if make_new:    #deletes saved network folder if it exists
            if (os.path.exists(self.saved_model_dir)):
                shutil.rmtree(self.saved_model_dir)

    def make_dense_model(self, input_size, output_size, name = None):
        model = TF_MODEL(name)

        inputs = model.input(input_size)
        
        layer = model.layer["dense"](inputs, neurons = 353, activation_func = "relu")

        output_layer = model.layer["dense"](layer, neurons = output_size)

        model.output(output_layer)

        model.define_cost(model_type  = "regression")
        model.define_optimizer(optimizer_type = "adam", learning_rate = 0.001)

        model.output_function(activation = "softmax", name = "output")

        #model.init()

        return model

    def make_prediction(self, state):
        return np.argmax(self.qmodel.get_prediction([state])[0])

    def make_valid_prediction(self, state):
        predictions = self.qmodel.get_out_func([state])[0]
        #print(predictions)
        if state.ndim == 1:
            valid_states = state[9:18]
        else:
            valid_states = state[:,:,1].flatten()
        
        for i, tile_state in enumerate(valid_states):
            if tile_state == 0:
                predictions[i] = -1

        return np.argmax(predictions)

    def get_prediction_probabilities(self, state):
        return self.qmodel.get_out_func([state])[0]

    def get_batch_Q_values(self, states):
        return self.tmodel.get_prediction(states)
        
    def get_Q_values(self, state):
        return self.qmodel.get_prediction([state])

    def train_batch(self, batch_x, batch_y):
        loss = self.qmodel.train(batch_x, batch_y)
        self.qmodel.execute(self.copy_op)
        return loss

    def train(self, state, target_output):
        return self.qmodel.train([state], target_output)

    def save(self):
        if (os.path.exists(self.saved_model_dir)):
            shutil.rmtree(self.saved_model_dir)
            os.makedirs(self.saved_model_dir)
        else:
            os.makedirs(self.saved_model_dir)

        self.qmodel.save(self.saved_model_dir)

    def load(self):
        if (os.path.exists(self.saved_model_dir)):
            self.qmodel.load(self.saved_model_dir)
            return 1
        else:        
            return 0  

    def terminate(self):
        self.qmodel.terminate_model()

    def convert_to_bitinput(self, board):
        flatboard = board.reshape(-1)

        bitarray = np.zeros(self.input_size)
        index = 0

        for state in self.tile_states:
            for i in range(len(flatboard)):
                if flatboard[i] == state:
                    bitarray[index] = 1
                index += 1
                
        return bitarray


    
