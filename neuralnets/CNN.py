import numpy as np
import os
import shutil

from neural_network.TFmodel import TF_MODEL

class MODEL():
    def __init__(self, input_size, output_size, name = None, make_new = False):
        self._model = self.make_cnn_model(input_size, output_size, name = name)
        self.input_size = input_size
        self.output_size = output_size

        self._model_name = None
        if name is None:
            self._model_name = "neural_net"
        else:
            self._model_name = name

        self.tile_states = [1,0,-1]

        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        model_path = os.path.join(THIS_FOLDER, "neural_network/saved_models") 

        self.saved_model_dir = os.path.join(model_path, self._model_name)

        if make_new:    #deletes saved network folder if it exists
            if (os.path.exists(self.saved_model_dir)):
                shutil.rmtree(self.saved_model_dir)
    
    def make_cnn_model(self, input_size, output_size, name = None):
        model = TF_MODEL(name)

        inputs = model.input(input_size)

        conv_layer_1 = model.layer["conv2d"](inputs, kernel_size = (3, 3), num_channels = 3, num_filters = 128, strides = [1,1,1,1], padding = 'SAME', activation_func = "relu")
        conv_layer_2 = model.layer["conv2d"](conv_layer_1, kernel_size = (3, 3), num_channels = 128, num_filters = 128, strides = [1,1,1,1], padding = 'SAME', activation_func = "relu")
        conv_layer_3 = model.layer["conv2d"](conv_layer_2, kernel_size = (3, 3), num_channels = 128, num_filters = 64, strides = [1,1,1,1], padding = 'SAME', activation_func = "relu")

        flattened_layer = model.reshape(conv_layer_3, [-1, 3 * 3 * 64])
        
        dense_layer = model.layer["dense"](flattened_layer, neurons = 576, activation_func = "relu")
        q_values = model.layer["dense"](dense_layer, neurons = output_size)

        model.output(q_values)

        model.define_cost(model_type  = "regression")
        model.define_optimizer(optimizer_type = "adam", learning_rate = 0.001)

        model.output_function(activation = "softmax", name = "output")

        model.init()

        return model

    def make_prediction(self, state):
        return np.argmax(self._model.get_prediction([state])[0])

    def make_valid_prediction(self, state):
        predictions = self._model.get_out_func([state])[0]
        #print(predictions)
        if state.ndim == 1:
            valid_states = state[9:18]
        else:
            valid_states = state[:,:,1].flatten()
        
        for i, tile_state in enumerate(valid_states):
            if tile_state == 0:
                predictions[i] = -1

        return np.argmax(predictions)

    def get_prediction_probabilities(self, state):
        return self._model.get_out_func([state])[0]

    def get_batch_Q_values(self, states):
        return self._model.get_prediction(states)
        
    def get_Q_values(self, state):
        return self._model.get_prediction([state])

    def train_batch(self, batch_x, batch_y):
        return self._model.train(batch_x, batch_y)

    def train(self, state, target_output):
        return self._model.train([state], target_output)

    def save(self):
        if (os.path.exists(self.saved_model_dir)):
            shutil.rmtree(self.saved_model_dir)
            os.makedirs(self.saved_model_dir)
        else:
            os.makedirs(self.saved_model_dir)

        self._model.save(self.saved_model_dir)

    def load(self):
        if (os.path.exists(self.saved_model_dir)):
            self._model.load(self.saved_model_dir)
            return 1
        else:        
            return 0  

    def terminate(self):
        self._model.terminate_model()

    def convert_to_bitinput(self, board):
        bitmatrix = np.zeros((self.input_size[2], self.input_size[1], self.input_size[0]))
        for state in range(self.input_size[2]):
            for row in range(self.input_size[1]):
                for column in range(self.input_size[0]):
                    if(board[row][column] == self.tile_states[state]):
                        bitmatrix[state][row][column] = 1
        bitmatrix = np.transpose(bitmatrix, [1,2,0])
        return bitmatrix
         


    
