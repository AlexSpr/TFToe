import numpy as np
import os
import shutil
import tensorflow as tf


class Model(tf.Module):
    def __init__(self, input_size, output_size, name = "model", make_new = False):
        self.model_name = name
        self.input_size = input_size
        self.output_size = output_size
        
        self.layer1_vars = { 'weights': tf.Variable(tf.initializers.GlorotUniform()([input_size, 565])), 'biases': tf.Variable(tf.initializers.GlorotUniform()([565]))}
        self.layer2_vars = { 'weights': tf.Variable(tf.initializers.GlorotUniform()([565, output_size])), 'biases': tf.Variable(tf.initializers.GlorotUniform()([output_size]))}

        self.loss_function = tf.losses.mean_squared_error
        self.optimizer = tf.optimizers.Adam(0.001)

        self.tile_states = np.array([1,0,-1])

        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        self.model_path = os.path.join(THIS_FOLDER, "saved", self.model_name) 

    @tf.function(input_signature=(tf.TensorSpec(shape=[None, 27], dtype=tf.float32),))
    def predict(self, x):
        layer1 = tf.add(tf.matmul(x, self.layer1_vars['weights'], name = "L1_matmul"), self.layer1_vars["biases"], name = "L1_add")
        layer1 = tf.nn.relu(layer1)
        output = tf.add(tf.matmul(layer1, self.layer2_vars['weights'], name = "L2_matmul"), self.layer2_vars['biases'], name = "L2_add")
        return output

    def predict_with_softmax(self, x):
        return tf.nn.softmax(self.predict(x))

    def make_valid_prediction(self, state):
        #print("State",state)
        predictions = self.predict_with_softmax([state])[0].numpy()
        #print("predictions",predictions)
        if state.ndim == 1:
            valid_states = state[9:18]
        else:
            valid_states = state[:,:,1].flatten() 
        for i, tile_state in enumerate(valid_states):
            if tile_state == 0:
                predictions[i] = -1
        #print("predictions cleaned",predictions)
        return np.argmax(predictions)

    def get_batch_Q_values(self, states):
        return self.predict(states)
    
    def get_Q_values(self, state):
        return self.predict([state])[0]
    '''
    def print_var(self):
        print(self.layer1_vars['weights'])
    '''
    @tf.function(input_signature=[tf.TensorSpec(shape=[None, 27], dtype=tf.float32),tf.TensorSpec(shape=[None, 9], dtype=tf.float32)])
    def train_batch_step(self, states, targets):
        with tf.GradientTape() as tape:
            predictions = self.predict(states)
            step_loss = self.loss_function(targets, predictions)

        gradients = tape.gradient(step_loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))
        return tf.reduce_mean(step_loss)

    def train_step(self, state, target):
        return self.train_batch_step([state], target)

    def save(self):
        tf.saved_model.save(self, self.model_path)

    def load(self):
        if (os.path.exists(self.model_path)):
            loaded_model = tf.saved_model.load(self.model_path)
            super().__dict__.update(loaded_model.__dict__)
            return 1
        else:        
            return 0
    '''
    def softmax(self, array):
        return np.exp(array) / np.sum(np.exp(array))
    '''
    def convert_to_bitinput(self, board):
        flatboard = board.reshape(-1)
        bitarray = np.zeros(self.input_size)
        index = 0

        for state in self.tile_states:
            for i in range(len(flatboard)):
                if flatboard[i] == state:
                    bitarray[index] = 1
                index += 1
        return bitarray
         


    
