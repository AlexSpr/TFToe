import numpy as np
import random
import decimal  


class TICTACTOEGAME():
    def __init__(self):
        self.board = []
        self.to_move = None
        self.num_of_moves = None
        self.gameOver = False
        self.winner = None

        self.rowIdentity = ['a','b','c']

    def initilize_game(self, player_begin_chance = 0.5):
        self.board = self.initilizeBoard()
        self.num_of_moves = 0
        self.gameOver = False
        self.winner = None

        coinFlip = random.random()	#generates float between 0 and 1 
        if coinFlip <= player_begin_chance:	# if value is below 0.5 the player begins game 
            self.to_move = -1 
        else:				# if value is above 0.5 the player begins game 
            self.to_move = 1

        return self.to_move

    def make_move(self, row_position, column_position = None):
        if column_position is None:
            full_index = row_position
            row_position = int(full_index/3)
            column_position = full_index % 3

        if self.board[row_position][column_position] == 0:
            self.board[row_position][column_position] =  self.to_move
        else:
            raise Exception("!!!A False Move Was Made!!! Row Position:", row_position, "Column Position:", column_position)
          
        game_state_reward = self.check_game_over() #outputs 1 if the game is done
        if game_state_reward:
            self.gameOver = game_state_reward
            self.winner = self.to_move
            return self.get_board(), game_state_reward * self.to_move    
        
        if self.num_of_moves + 1 == 9:
            self.gameOver = True
            self.winner = 0
            #game_state_reward = 0.5
            return self.get_board(), game_state_reward

        self.num_of_moves += 1
        self.to_move *= -1

        return self.get_board(), game_state_reward

    def make_random_move(self):
        viable_moves = self.get_viable_moves()
        return self.make_move(random.choice(viable_moves))

    def rotate_board(self):
        self.board = np.rot90(self.board, k = random.randint(1,4))
    
    def get_viable_moves(self):
        viable_moves = []
        index = 0
        for row in self.board:
            for  column in row:
                if column == 0:
                    viable_moves.append(index)
                index += 1
        return viable_moves

    def get_board(self):
        return np.copy(self.board)

    def get_winner(self):
        return self.winner

    def isMoveViable(self, row_position, column_position = None):	# check if move is viable
        if column_position is None:
            full_index = row_position
            row_position = int(full_index/3)
            column_position = full_index % 3

        if self.board[row_position][column_position] != 0 :	# move is only viable if field is empty 
            return False
        else:
            return True

    def initilizeBoard(self): # creates 3 x 3 matrix 
        w, h = 3, 3 
        matrix = [[0 for x in range(w)] for y in range(h)]
        return matrix

    def printCurrentBoard(self): #print self.board with guides for player 
        print_board = [[' ' for x in range(3)] for y in range(3)]

        for i in range(len(self.board)):
            for j in range(len(self.board[i])):  
                if self.board[i][j] == 0:
                    print_board[i][j] = ' '
                elif self.board[i][j] == 1:
                    print_board[i][j] = 'o'
                else:
                    print_board[i][j] = 'x'

        print (' 1  2  3')
        for i, row in enumerate(print_board):
            rowString = self.rowIdentity[i] + '|'
            for cell in row:
                rowString = rowString + cell + '|'
            print(rowString)

    def check_game_over(self): # checks for tie or winner
        for row in self.board:   #check row 
            if len(set(row)) <= 1 and row[0] != 0: # there is a winner if row is full 
                return 1#, row[0] # report winner 
        for column in np.rot90(self.board):  #check column for winner
            if len(set(column)) <= 1 and column[0] != 0:    # there is a winner if column is full 
                return 1#, column[0]  # report winner 

        if len(set([self.board[0][0],self.board[1][1],self.board[2][2]])) <= 1 and self.board[0][0] != 0:   # there is a winner if diagonal is full  
            return 1#, self.board[0][0] # report winner 
        if len(set([self.board[0][2],self.board[1][1],self.board[2][0]])) <= 1 and self.board[0][2] != 0:   # there is a winner if diagonal is full  
            return 1#, self.board[0][2]    # report winner 

        return 0

if __name__ == "__main__":
    win_dic = dict([(1, "NN AI"), (-1, "Player"), (0, "None, it's a tie")])
    ttt = TICTACTOEGAME()
    to_move = ttt.initilize_game()
    print(win_dic[to_move], "starts")
    ttt.printCurrentBoard()
    while not ttt.gameOver:
        text = str(input("Please make move (such as a1, b3, etc ...):"))
        _, reward = ttt.make_move(text[0], text[1])
        ttt.printCurrentBoard()

    print(reward)
        