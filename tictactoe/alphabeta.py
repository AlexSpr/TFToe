import numpy as np
import copy

class AlphaBeta():

    def __init__(self):
        self.toMove = []

    def alpha_beta(self, board):	# start alpha beta pruning with max and initalize alpha beta 
        value = self.maxValue(board, -100, 100, 0)
        #print('Value of move:', value)

    def maxValue(self, board, alpha, beta, depth):
        #global toMove	# ideal move to be made 
        gameStatus, winner = self.gameOver(board)	#checks if current board has winner or tie 
        if gameStatus:
            if winner == 1:	# if algorithm is winner return 1 
                return 1
            elif winner == -1: # if player is winner return -1
                return -1
            elif winner == 0: # if tie return 0 
                return 0 

        value = -100 #initalize value to - infinity 
        listMoves = self.generateMoves(board) #create all possible moves for current board 
        for move in listMoves:#while listMoves:	# for generated moves 
            copiedboard = copy.deepcopy(board) # make copy of board 

            nextMove = move#listMoves.pop(0) #get first move 
            row, column = nextMove[0], nextMove[1]	#make move on copied board 
            copiedboard[row][column] = 1 #make move on copied board 
            v = self.minValue(copiedboard, alpha, beta, depth + 1) # - depth	#pass copied board to minValue
            if v > value:	#if returned min value is larger then initalized value 
                if depth == 0:
                    self.toMove = nextMove #nextMove 
                value = v #returned value is new value 
            if value >= beta:	# if value is larger than beta prune and return value 
                return value 
            alpha = max(alpha, value) # pick larger value 
        return value

    def minValue(self, board, alpha, beta, depth):
        #global toMove # ideal move to be made
        gameStatus, winner = self.gameOver(board) #checks if current board has winner or tie 
        if gameStatus:
            if winner == 1:	# if algorithm is winner return 1 
                return 1
            elif winner == -1: # if player is winner return -1
                return -1
            elif winner == 0: # if tie return 0 
                return 0  

        value = 100 #initalize value to infinity 
        listMoves = self.generateMoves(board)	#create all possible moves for current board
        for move in listMoves:#while listMoves:	# for generated moves 
            copiedboard = copy.deepcopy(board)	# make copy of board 
            nextMove = move#listMoves.pop(0)	 #get first move 
            row, column = nextMove[0], nextMove[1]	#make move on copied board 
            copiedboard[row][column] = -1	#make move on copied board 
            v = self.maxValue(copiedboard, alpha, beta, depth + 1) #+ depth# + depth	#pass copied board to maxValue 
            if v < value:	#if returned min value is larger then initalized value
                value = v #returned value is new value 
            if value <= alpha:	# if value is smaller than alpha prune and return value  
                return value 
            beta = min(beta, value)	# pick smaller value
        return value 

    def choose_move(self, board): #algortihm move 
        self.alpha_beta(board)  
        return self.toMove[0], self.toMove[1] # return updated board 

    def generateMoves(self, board):	# generates all possible moves on given board by going through each row and column 
        possibleMoves = []
        for i, row in enumerate(board):
            for j in range(len(row)):
                if self.isMoveViable(board,i,j):		# checks if board position is already taken 
                    possibleMoves.append([i,j])	# if not add move to possible moves 
        return possibleMoves	# return moves 

    def gameOver(self, board): # checks for tie or winner
        for row in board:	#check row 
            if len(set(row)) <= 1 and row[0] != 0: # there is a winner if row is full 
                return True, row[0]	# report winner 
        for column in np.rot90(board):	#check column for winner
            if len(set(column)) <= 1 and column[0] != 0:	# there is a winner if column is full 
                return True, column[0]	# report winner 

        if len(set([board[0][0],board[1][1],board[2][2]])) <= 1 and board[0][0] != 0:	# there is a winner if diagonal is full  
            return True, board[0][0] # report winner 
        if len(set([board[0][2],board[1][1],board[2][0]])) <= 1 and board[0][2] != 0:	# there is a winner if diagonal is full  
            return True, board[0][2]	# report winner 
        
        totalCount = 0 	#check for tie 
        for row in board: 
            totalCount += list(row).count(-1) + list(row).count(1)
        if totalCount == 9:
            return True, 0

        return False, None  

    def isMoveViable(self, board, row, column):	# check if move is viable 
        if board[row][column] != 0 :	# move is only viable if field is empty 
            return False
        else:
            return True