import numpy as np

def softmax(array):
    return np.exp(array) / np.sum(np.exp(array))

def softmax2(array):
    ex = np.exp(array - np.max(array))

def softmax3(z):
    assert len(z.shape) == 2
    s = np.max(z, axis=1)
    s = s[:, np.newaxis] # necessary step to do broadcasting
    e_x = np.exp(z - s)
    div = np.sum(e_x, axis=1)
    div = div[:, np.newaxis] # dito
    return e_x / div

arr  = [0, 154, 0, 43]
arr2 = np.array([.050, 0.150, 0.30, 0.20, 0.20, 0.10])

#print(sum(arr2))
print(np.sum(arr))
print(arr)
print(softmax(arr))
print(np.sum(softmax(arr)))

print("Softmax 3")
print(np.sum(arr))
print(arr)
print(softmax3(np.array([arr])))
print(np.sum(softmax3(np.array([arr]))))


#print(softmax(arr))
#print(np.sum(softmax(arr)))
