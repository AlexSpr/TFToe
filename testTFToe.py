import random
import numpy as np

from utilities.plot import Plot

import NN
from TicTacToeGame import TICTACTOEGAME
from utilities.alphabeta import AlphaBeta

class TFTOE_TESTER():
    def __init__(self):
        self.nn = NN.MODEL((3,3,3),9, name = "CNN")

        if (self.nn.load()):
            print("Loaded Model")
        else:
            raise Exception("Model to load not found")

        self.winner_dict = { 1 : 0, 0 : 0, -1: 0}

    def ab_test_game(self):
        ab = AlphaBeta()

        ttt = TICTACTOEGAME()
        ttt.initilize_game()

        while not ttt.gameOver:
            ttt.rotate_board()
            if ttt.to_move == 1: # if it's nn move
                #print("NN move")
                action = self.nn.make_valid_prediction(self.nn.convert_to_bitinput(ttt.get_board()))
                action2 = None 
            else:   
                #print("AB move")
                action, action2 = ab.choose_move(np.negative(ttt.get_board()))

            ttt.make_move(action, action2) #adds action to board and switches ttt.to_move to other player
            #ttt.printCurrentBoard()
        
        self.winner_dict[ttt.get_winner()] += 1

    def tftoe_test_run(self, num_games = 1000):
        plot = Plot()
        for game in range(num_games):
            print("Game:", game + 1,"\tof", num_games, end = '\r')
            self.ab_test_game()
            plot.update_values(self.winner_dict[1], self.winner_dict[-1], self.winner_dict[0], 0, self.winner_dict[0])
        print(self.winner_dict)
        plot.update_dynamic()
        plot.keep_open()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-new', action = "store_true" , help = 'Create new nn to train', required = False, default = False)
    parser.add_argument('-save_plots', action = "store_true" , help = 'Save plots to folder?', required = False, default = False)
    parser.add_argument('-find_perfect', action = "store_true" , help = 'Train till perfect player is found', required = False, default = False)
    parser.add_argument('-runs', "--training_runs" , help = 'How often should the nn be trained', required = False, default = 1)
    parser.add_argument('-name', "--model_name" , help = 'Name of Model', required = False, default = "Model")
    
    args = parser.parse_args()
    new_nn = args.new
    runs = int(args.training_runs)
    perfect = args.find_perfect
    save_plots = args.save_plots
    model_name = str(args.model_name)



    tftoe = TFTOE_TESTER()
    #tftoe.ab_test_game()
    tftoe.tftoe_test_run()