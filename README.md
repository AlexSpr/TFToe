# TFTOE  
**training a neural network to become an unbeatable tic-tac-toe player with Q-Learning and self-play.**

## Requirements  
- python 3.6+ 
- TensorFlow 1.8+
- matplotlib
- numpy 

## Project Main Scripts
*trainTFToe.py*  
This script trains the neural network   

*playTFToe.py*  
This script allows you to play against the trained neural network  

*{any}NN.py*  
This script holds the descritption of the neural network  

## train neural network
```bash
python3 trainTFToe.py -new -find_perfect -name myModelName -save_plots(optional)  
```
- "new" -> creates a new network with given name. If the argument is not given and a neural network of the name already
exists it will be loaded and trained
- "find_perfect" -> Continue with training runs until a perfect player is found 
- "name" -> Give a name under which the network will be saved. The stored network can be found
in the neural_network/saved_models/myModelName directory

## play against neural network  
```bash
python3 playTFToe.py -name myModelName  
```
- "name" -> input the name of the neural network that you want to play against 

