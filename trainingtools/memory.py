import random
import copy

class Memory:
    def __init__(self, max_memory):
        self.max_memory = max_memory
        self.samples = []
        self.scored_samples = []
    
    def add_sample(self, sample):
        self.samples.append(copy.deepcopy(sample))
        if len(self.samples) > self.max_memory:
            self.samples.pop(0)

    def get_sample(self, no_samples):
        if no_samples > len(self.samples):
            return random.sample(self.samples, len(self.samples))
        else: 
            return random.sample(self.samples, no_samples)

    def get_scored_sample(self, no_samples):
        if no_samples > len(self.scored_samples):
            return random.sample(self.scored_samples, len(self.scored_samples))
        else: 
            return random.sample(self.scored_samples, no_samples)

    def score_samples(self):
        self.scored_samples = copy.deepcopy(self.samples)

        index = len(self.scored_samples) - 1
        game_index = 1

        while index >= 0:
        #for index in range(len(self.scored_samples)):
            if self.scored_samples[index][3] == None:
                 main_reward = self.scored_samples[index][2]
                 game_index = 1
            else: 
                self.scored_samples[index][2] = main_reward / (2 ** game_index)
                game_index += 1
            
            index -= 1 

    def delete_all_samples(self):
        del self.samples[:]
        del self.scored_samples[:]