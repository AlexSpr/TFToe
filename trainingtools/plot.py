import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

import numpy as np

class Plot():
    def __init__(self):
        plt.ioff()
        #self.fig = plt.figure(figsize=(6,3))
        self.ax1, self.axarr = plt.subplots(1, 2, figsize= (15,8))
        #plt.figure()
        self.label_set = False

        self.stack_plot_colors = ['green', 'red', 'yellow']

        self.wins = []
        self.loses = []
        self.draws = []

        self.e_value = []
        self.epoch_loss = []

    def keep_open(self):
        plt.show()

    def update_dynamic(self):
        self.axarr[0].plot(self.wins, '#29f25b', label = "wins")
        self.axarr[0].plot(self.loses, 'red', label = "loses")
        self.axarr[0].plot(self.draws, '#ffe100', label = "draws")
        self.axarr[0].plot(self.e_value, 'k', label = "e_value")

        self.axarr[1].plot(self.epoch_loss, 'red', label = "epoch loss")
        
        if not self.label_set:
            
            self.ax1.set_facecolor('#181e28')

            self.axarr[0].set_facecolor('#5b7399')

            self.axarr[0].spines['bottom'].set_color('white')
            self.axarr[0].spines['top'].set_color('white')
            self.axarr[0].spines['left'].set_color('white')
            self.axarr[0].spines['right'].set_color('white')

            self.axarr[0].xaxis.label.set_color('white')
            self.axarr[0].yaxis.label.set_color('white')

            self.axarr[0].set_title("game outcomes & e value", color = 'white')

            self.axarr[0].legend(loc='upper left')
            self.axarr[0].tick_params(axis='both', colors='white')
            self.axarr[0].set(xlabel='epochs', ylabel='Percentage')

            self.axarr[1].set_facecolor('#5b7399')

            self.axarr[1].xaxis.label.set_color('white')
            self.axarr[1].yaxis.label.set_color('white')

            self.axarr[1].spines['bottom'].set_color('white')
            self.axarr[1].spines['top'].set_color('white')
            self.axarr[1].spines['left'].set_color('white')
            self.axarr[1].spines['right'].set_color('white')

            self.axarr[1].set_title("epoch loss", color = 'white')

            self.axarr[1].legend(loc='upper left')
            self.axarr[1].tick_params(axis='both', colors='white')
            self.axarr[1].set(xlabel='epochs', ylabel='loss')

            self.label_set = True

    def draw(self):
        plt.draw()
        plt.pause(0.01)

    def update_values(self, wins, loses, draws, e_value, epoch_loss):
        self.wins.append(wins)
        self.loses.append(loses)
        self.draws.append(draws)

        self.e_value.append(e_value)
        self.epoch_loss.append(epoch_loss)
        #self.update_dynamic()

    def save(self, path):
        self.ax1.savefig(path, facecolor=self.ax1.get_facecolor(), edgecolor='none')

    